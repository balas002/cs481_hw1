import 'package:flutter/material.dart';

void main() => runApp(new MyApp());//one-line function

class MyApp extends StatelessWidget {
  //Stateless = immutable = cannot change object's properties
  //Every UI components are widgets
  @override
  Widget build(BuildContext context) {
    //Now we need multiple widgets into a parent = "Container" widget
    Widget titleSection2 = new Container(
      padding: const EdgeInsets.all(10.0),//Top, Right, Bottom, Left
      child: new Row(

        children: <Widget>[
          new Icon(Icons.favorite, color: Colors.red,size: 40.0,),
        ],

      ),
    );
    Widget titleSection = new Container(
      padding: const EdgeInsets.all(15.0),//Top, Right, Bottom, Left
      child: new Row(

        children: <Widget>[

          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(

                  padding: const EdgeInsets.only(bottom: 10.0),

                  child: new Text("Sunset Cliffs, San Diego",

                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 22.0
                      )),

                ),


                //Need to add space below this Text ?

                 new Text("\nTo book a cruise tour contact the following: ",
    style: new TextStyle(
    color: Colors.grey[850],
    fontSize: 17.0
    )),
                new Row(

                  children: <Widget>[

                    new Icon(Icons.phone, color: Colors.green,size: 50.0,),
                    new Text("    (760)-772-5683",
                        style: new TextStyle(
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                        fontSize: 18.0
                           )),

                  ],
                ),
                //     " The neighborhood is almost entirely residential, with curved contour-following streets and custom homes. "
                //     "A small commercial strip is found along the northern end of Sunset Cliffs Blvd. running into Ocean Beach. "
                // ),
              ],

            ),

          ),

        ],

      ),

    );
    //build function returns a "Widget"
    return new MaterialApp(
        title: "",
        home: new Scaffold(
            appBar: new AppBar(
              title: new Text('Flutter App'),
            ),
            body: new ListView(
              children: <Widget>[

                new Image.asset(
                    'images/sunset.JPG',
                    fit: BoxFit.cover
                ),


                //You can add more widget bellow
                titleSection

              ],
            )

        )
    );//Widget with "Material design"
  }
}